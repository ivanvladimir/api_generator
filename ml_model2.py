from llama_cpp import Llama

model = None
tokenizer = None

def init_transformer(model_name, tokenizer_name=None, token=None):
    global model
    global tokenizer

    if tokenizer_name is None:
        tokenizer_name = model_name

    model = Llama(model_path=model_name,n_ctx=1500)

    return model


def pre_process(text, to_lower=False):
    # If transform to lower
    if to_lower:
        text = text.lower()
    return text


def generate_transformer(text, text_length=50, do_sample=True, temperature=0.7, repetition_penalty=2.0, **args):
    global model
    global tokenizer

    text_ = pre_process(text, **args)
    res = model(text_, max_tokens=text_length, stop=[], echo=True)

    return res['choices'][0]['text'], (text_, )
