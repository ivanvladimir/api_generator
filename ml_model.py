from transformers import AutoTokenizer, AutoModelForCausalLM, pipeline

model = None
tokenizer = None

def init_transformer(model_name, tokenizer_name=None, token=None):
    global model
    global tokenizer

    if tokenizer_name is None:
        tokenizer_name = model_name

    tokenizer = AutoTokenizer.from_pretrained(tokenizer_name, cache_dir=".cache/models", trust_remote_code=True, token=token)
    model = AutoModelForCausalLM.from_pretrained(model_name, cache_dir=".cache/models", trust_remote_code=True, token=token)

    return model


def pre_process(text, to_lower=False):
    # If transform to lower
    if to_lower:
        text = text.lower()
    return text


def generate_transformer(text, text_length=50, do_sample=True, temperature=0.7, repetition_penalty=2.0, **args):
    global model
    global tokenizer

    text_ = pre_process(text, **args)
    input_ids = tokenizer(text_, return_tensors="pt").input_ids
    outputs = model.generate(input_ids, 
            do_sample=do_sample,
            temperature=temperature,
            repetition_penalty=repetition_penalty,
            max_length=text_length, 
            pad_token_id=tokenizer.eos_token_id)

    res=tokenizer.batch_decode(outputs, skip_special_tokens=True)

    return res[0], (text_, )
