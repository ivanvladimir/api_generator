from tinydb import TinyDB, Query
import uuid
import arrow
from functools import lru_cache

class TinyDBAccess:
    """ Tiny DB Access mainly for API Keys"""
    def __init__(self, tinydb_location, api_key_expiration_limit):
        """ Initializes de DB

        Arguments:
            tinydb_location Path to the database file
            api_key_expiration_limit    Time in years for the api key expiration"""
        try:
            self.db_location = tinydb_location
        except KeyError:
            self.db_location = "db/apikeys.db"

        try:
            self.expiration_limit_years = api_key_expiration_limit
        except KeyError:
            self.expiration_limit_years = 3

        self.db = TinyDB(self.db_location)
        self.Key = Query()

    def create(self, never_expire: bool):
        """ Creates and adds a new API key
        
            Arguments:
                never_exprie: If the API Key should expire
        """
        api_key = str(uuid.uuid4())
        arr=arrow.utcnow()
        self.db.insert({
            'key':api_key,
            'is_active':True,
            'never_expire':never_expire,
            'expiration_time': arr.shift(years=+3).isoformat(),
            'latest_query': None,
            'latest_renew': None,
            'total_queries': 0,
            'created':arr.isoformat()
            })
        return api_key

    def renew(self, api_key: str):
        """ Renews an API Key
        
        Arguments:
            api_key: API Key to renew"""
        arr=arrow.utcnow()
        self.db.update(
            {
                'latest_renew':arr.isoformat(),
                'expiration_time': arr.shift(years=+3).isoformat()
                },
            self.Key.key==api_key)
        return api_key

    def revoke(self, api_key: str):
        """ Renews an API Key 

        Arguments:
            api_key: API Key to revoke"""
        self.db.update(
            {
                'active':False,
                },
            self.Key.key==api_key)

    def verify(self, api_key: str):
        """ Verify an API Key
        
        Arguments:
            api_key: API Key to verify
        """
        arr=arrow.utcnow()
        res=self.db.search(
            self.Key.key==api_key)

        if len(res)==1:
            self.db.update(
                {
                'latest_query':arr.isoformat(),
                'total_queries': res[0]['total_queries']+1
                },
            self.Key.key==api_key)
            return True
        else:
            return False

    def stats(self, api_key: str):
        """ Stats of an API Key
        
        Arguments:
            api_key: API Key to recover statistics
        """
        res=self.db.search(
            self.Key.key==api_key)
        if len(res)==1:
            return res[0]
        else:
            return {}

db=None

def init_db(db_location,api_key_expiration_limit):
    global db
    db=TinyDBAccess(db_location, api_key_expiration_limit)
    return db

def get_db():
    return db

