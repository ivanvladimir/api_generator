from fastapi import Security
from fastapi.security import APIKeyQuery, APIKeyHeader
from starlette.exceptions import HTTPException
from starlette.status import HTTP_403_FORBIDDEN
from db import get_db

API_KEY_NAME = "access-token"

api_key_header = APIKeyHeader(name=API_KEY_NAME, auto_error=False)

class ApiKeySecurity():
    """Dependency for validate a key"""
    def __init__(self, admin_key):
        self.admin_key=admin_key

    def __call__(self,
        header_param: str = Security(api_key_header)
    ):
        db=get_db()
        if not header_param:
            raise HTTPException(status_code=HTTP_403_FORBIDDEN, detail="A valid _Access token_ should be provided")
        elif header_param and len(self.admin_key)>0 and (header_param==self.admin_key or db.verify(header_param)):
            return header_param
        else:
            raise HTTPException(status_code=HTTP_404_FORBIDDEN, detail="API key expired, wrong o rovoked")

