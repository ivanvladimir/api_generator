from typing import (Optional, List, Union)
from pydantic import BaseModel

from fastapi import FastAPI, Request, Depends, Header, UploadFile, File, Form
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from typing_extensions import Annotated

from security import ApiKeySecurity
from ml_model2 import init_transformer, generate_transformer
from db import init_db

from functools import lru_cache
import arrow
import os

__VERSION__ = "0.0.1"
__API_NAME__ = "Generation API"

app = FastAPI()

class PreprocessTextOptions(BaseModel):
    """Options to configure the pre-processing of text

    Options:
        use_lower   Lower the tweet
    """
    use_lower: Optional[bool] = False


class TextOptions(PreprocessTextOptions):
    """Arguments and options for text generation

    Aguments:
        text   Text to classify
    """
    prompt: str
    length: Optional[int] = 50

class APIKeyArguments(BaseModel):
    """API key as argument

    Aguments:
        api_key     String of the API Key
    """
    api_key: str


def create_app(test_config=None):
    START_TIME = arrow.utcnow()
    STATUS = "active"

    # Arrange configuration
    import config

    @lru_cache()
    def get_settings():
        return config.Settings()

    def elapsed_time(start_time):
        '''Calculates the elapsed time'''
        return (arrow.utcnow() - start_time).total_seconds()

    settings = get_settings()

    # Sets the APIKey security, it will be handled asdependecy of the protected calls
    api_key_security=ApiKeySecurity(settings.ADMIN_KEY)

    # Gets DB
    db=init_db(settings.DB_LOCATION, settings.API_KEY_EXPIRATION_LIMIT)

    # Gets model
    model_ = init_transformer(settings.API_GENERATE_MODEL_NAME, token=settings.HF_TOKEN)

    app = FastAPI()
    app.mount(f"/{settings.API_GENERATE_URL_PREFIX}/static", StaticFiles(directory="static"), name="static")
    templates = Jinja2Templates(directory="templates")

    api_ = FastAPI()
    apikey_ = FastAPI()

    @app.get("/", response_class=HTMLResponse)
    def home(request: Request):
        """Renders main page"""
        return templates.TemplateResponse("base.html", 
                {'request':request,
                 'model_name': settings.API_GENERATE_MODEL_NAME})

    @api_.post("/x/generate",
            dependencies=[Depends(api_key_security)])
    def htxm_generate(request: Request, 
            prompt: Annotated[str, Form()], 
            length: Annotated[int, Form()],
            temperature: Annotated[float, Form()],
            repetition_penalty: Annotated[float, Form()],
            do_sample: Annotated[Union[bool,None], Form()] = False
            ):
        """Generate text"""
        start_time = arrow.utcnow()
        res, (text_) = generate_transformer(prompt, text_length = length)

        return templates.TemplateResponse("result.html", 
                {'request':request,
                 'generated_text': res,
                 'elapsed_time': f'{elapsed_time(start_time):2.1f} segs'})
            
    @api_.get("/status")
    def status():
        """Prints status of API"""
        diff = arrow.utcnow() - START_TIME
        days = diff.days
        hours, remainder = divmod(diff.seconds, 3600)
        minutes, seconds = divmod(remainder, 60)
        return {
            "name": __API_NAME__,
            "version": __VERSION__,
            "model": settings.API_GENERATE_MODEL_NAME,
            "model_loaded": True if model_ else False,
            "status": STATUS,
            "uptime": f"Elapsed Time: {days} Days, {hours} Hours, {minutes} Minutes, {seconds} Seconds.",
        }

    @api_.post("/generate", 
            dependencies=[Depends(api_key_security)])
    def generate(info: TextOptions):
        """Generate text"""
        start_time = arrow.utcnow()
        res, (text_) = generate_transformer(info.prompt, use_lower=info.use_lower, text_length = info.length)

        return {
            "prompt": info.prompt,
            "proseced_prompt": text_,
            "max_length": info.length,
            "result": res,
            "elapsed_time": f"{elapsed_time(start_time):2.4f} segs",
        }

    @apikey_.post('/create',
            dependencies=[Depends(api_key_security)])
    def api_key_create(never_expires=False):
        """Creates a new API Key but it requieres a valid API Key as an access token
        
        Arguments:
            access_token    Header argument of POST call with the API Key that allows access
            never_expires   JSON param that specifies if API Key expires
        """
        return db.create(never_expires)

    @apikey_.post('/renew',
            dependencies=[Depends(api_key_security)])
    def api_key_renew(api_key: APIKeyArguments):
        """Renews an API Key but it requieres a valid API Key as an access token
        
        Arguments:
            access_token    Header argument of POST call with the API Key that allows access
            api_key         JSON param that specifies the API Key to renew
        """
        if len(api_key.api_key)>0:
            return db.renew(api_key.api_key)
        else:
            return {"status":"error",
                    "message":"No API key provided"}

    @apikey_.post('/revoke',
            dependencies=[Depends(api_key_security)])
    def api_key_revoke(api_key: APIKeyArguments):
        """Revokes an API Key but it requieres a valid API Key as an access token
        
        Arguments:
            access_token    Header argument of POST call with the API Key that allows access
            api_key         JSON param that specifies the API Key to revoke
        """

        if len(api_key.api_key)>0:
            return db.revoke(api_key.api_key)
        else:
            return {"status":"error",
                    "message":"No API key provided"}

    @apikey_.post('/stats',
            dependencies=[Depends(api_key_security)])
    def api_key_stats(api_key: APIKeyArguments):
        """Gets statistics of use of an API Key but it requieres a valid API Key as an access token
        
        Arguments:
            access_token    Header argument of POST call with the API Key that allows access
            api_key   JSON param that specifies the API Key to get statistics from
        """
        if len(api_key.api_key)>0:
            return db.stats(api_key.api_key)
        else:
            return {"status":"error",
                    "message":"No API key provided"}

    app.mount(f"/api", api_)
    app.mount(f"/api_key", apikey_)
    return app

app = create_app()
