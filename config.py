from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    API_GENERATE_URL_PREFIX: str = "iimas"
    API_GENERATE_MODEL_NAME: str = "ai-forever/mGPT"

    DB_LOCATION: str = "apikeys.db"
    API_KEY_EXPIRATION_LIMIT: int = 1
    ADMIN_KEY: str = ""
    HF_TOKEN: str = ""

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"
